#!/usr/bin/python3

"""
This is a copy of entropy, but to compute prior probability of output priorO
"""

"""
defines the following functions
jwae(xa, xt) : joint weighted average entropy for an attackers' and a targets' sets of inputs
awae(xa) : wae for an attackers' set of inputs
twae(xt) : wae for a targets' set of inputs

uses log2 to be consistent with bits of entropy
"""

###

"""
usage: e = entropy.wae(f, pYs, pZs, X)

- f still takes an arbitrary number of arguments

    changes from ../duplicates/entropy_analysis.py:
- number or attackers or victims is arbitrary (not only 1)
- prior probability distributions for those can also be precised (not only uniform)
- not necessarily the same distribution for all parties
"""


### notations ###

# X : attackers (vector)
# Y : victims (vector)
# Z : other parties (vector)
# CAPITAL letters denote vectors


### imports ###

import itertools
from functools import reduce
import operator
import inspect
import sys
import math
#from scipy.stats import entropy as entt

iproduct = itertools.product

### defs ###

# same as sum
def prod(X):
  return reduce(operator.mul, X, 1)

# entropy of an iterator of probabilities
# that must add up to 1
# added guard p>0 for safety
def entropy(S):
  return - sum(p*math.log2(p) for p in S if p > 0)

def min_entropy(S):
  return - math.log2(max(p for p in S))

# generic entropy (wae) computation
# f : function
# pYs : prior distributions for the Ys (list of dictionaries)
# pZs : prior distributions for the Zs (list of dictionaries)
# X : a given vector X (as a tuple)

def priorO(f, pYs, pZs, X):
  # precondition
  # bypass precondition if number of arguments == 0, i.e. if f is defined as a function of another function
  if len(X) + len(pYs) + len(pZs) != len(inspect.getargspec(f)[0]) and len(inspect.getargspec(f)[0]) != 0:
    print("len(X) = {}".format(len(X)))
    print("len(pYs) = {}".format(len(pYs)))
    print("len(pZs) = {}".format(len(pZs)))
    print("len(f) = {}".format(len(inspect.getargspec(f)[0])))
    sys.exit("Oops: Number of arguments didn't match")
  
  if len(inspect.getargspec(f)[0]) == 0:
    try:
      a = f(*((0,)*(len(X) + len(pYs) + len(pZs))))
    except:
      sys.exit("Oops2: Number of arguments didn't match")
  
  # prior distribution for Output
  pO = dict()
  # joint distribution for Output and Y
  pOaY = dict()
  for Y in itertools.product(*pYs):
    for Z in itertools.product(*pZs):
      o = f(*(X+Y+Z))
      p_o = prod( (pYZ[yz] for (yz, pYZ) in zip(Y+Z, pYs+pZs)) )
      # critical for distributions with a zero probability!
      # next line ensures that only possible events appear in pO (the if)
      if p_o > 0:
        if o in pO:
          pO[o] += p_o
          if Y in pOaY[o]:
            pOaY[o][Y] += p_o
          else:
            pOaY[o][Y] = p_o
        else:
          pO[o] = p_o
          pOaY[o] = {Y: p_o}
        
  return pO

# weighted min entropy (attackers')
def wme(f, pYs, pZs, X):
  # precondition
  # bypass precondition if number of arguments == 0, i.e. if f is defined as a function of another function
  if len(X) + len(pYs) + len(pZs) != len(inspect.getargspec(f)[0]) and len(inspect.getargspec(f)[0]) != 0:
    print("len(X) = {}".format(len(X)))
    print("len(pYs) = {}".format(len(pYs)))
    print("len(pZs) = {}".format(len(pZs)))
    print("len(f) = {}".format(len(inspect.getargspec(f)[0])))
    sys.exit("Oops: Number of arguments didn't match")
  
  if len(inspect.getargspec(f)[0]) == 0:
    try:
      # unexpected exception when domain of f is restricted!
      #a = f(*((0,)*(len(X) + len(pYs) + len(pZs))))
      a = f( *( X + tuple(list(pp.keys())[0] for pp in pYs) + tuple(list(pp.keys())[0] for pp in pZs)) )
    except KeyError:
      # here we tolerate KeyError because merged functions might not recognise input (0, 0, ...)
      pass
    except:
      sys.exit("Oops2: Number of arguments didn't match")
  
  # prior distribution for Output
  pO = dict()
  # joint distribution for Output and Y
  pOaY = dict()
  for Y in itertools.product(*pYs):
    for Z in itertools.product(*pZs):
      o = f(*(X+Y+Z))
      p_o = prod( (pYZ[yz] for (yz, pYZ) in zip(Y+Z, pYs+pZs)) )
      # critical for distributions with a zero probability!
      # next line ensures that only possible events appear in pO (the if)
      if p_o > 0:
        if o in pO:
          pO[o] += p_o
          if Y in pOaY[o]:
            pOaY[o][Y] += p_o
          else:
            pOaY[o][Y] = p_o
        else:
          pO[o] = p_o
          pOaY[o] = {Y: p_o}
        
  ### then convert pOaY into pYgO (kinda Bayes)
  # entropiesY stores the entropy of Y given output
  #entropiesY = dict()
  # maxsY stores the maximal probability of each distribution of pYgO for each o
  maxsY = dict()
  for output in pO: # or in pOgY, equivalently
    for Y in pOaY[output]:
    #for Y in itertools.product(*pYs):
      #p_y = prod( (pY[y] for (y, pY) in zip(Y, pYs)) )
      pOaY[output][Y] /= pO[output]
    # min_entropy here
    #entropiesY[output] = min_entropy(pOaY[output].values())
    maxsY[output] = max(pOaY[output].values())
    #
    #if (len(pOaY[output].values())>1):
    #  print("hello: {}".format(pOaY[output].values()))
    
  # weighted average entropy
  #wme = sum(pO[output]*entropiesY[output] for output in pO)
  wme = - math.log2( sum(pO[output]*maxsY[output] for output in pO) )
  
  return wme

# A attackers
# T targets
# S spectators
# O output

### p(O|A) ###
def pOgA(f, pYs, pZs, X):
  pO = dict()
  for Y in itertools.product(*pYs):
    for Z in itertools.product(*pZs):
      o = f(*(X+Y+Z))
      p_o = prod( (pYZ[yz] for (yz, pYZ) in zip(Y+Z, pYs+pZs)) )
      if o in pO:
        pO[o] += p_o
      else:
        pO[o] = p_o
  return pO

### p(O|T) ###
# arguments:
# pXs: prior that T has on A
# pZs: prior that T has on S
# T:   T's set on inputs
def pOgT(f, pXs, pZs, T):
  pO = dict()
  for X in itertools.product(*pXs):
    for Z in itertools.product(*pZs):
      o = f(*(X+T+Z))
      p_o = prod( (pXZ[xz] for (xz, pXZ) in zip(X+Z, pXs+pZs)) )
      if o in pO:
        pO[o] += p_o
      else:
        pO[o] = p_o
  return pO

### p(O|AT) ###
# pZs: prior that all have on S
# A:   A's inputs
# T:   T's inputs
def pOgAT(f, pZs, A, T):
  pO = dict()
  for Z in itertools.product(*pZs):
    o = f(*(A+T+Z))
    p_o = prod( (pZ[z] for (z, pZ) in zip(Z, pZs)) )
    if o in pO:
      pO[o] += p_o
    else:
      pO[o] = p_o
  return pO

### entropyTgOA ###
# entropy of T for a given output #
# implicitly computes p(Y|OA) and takes their entropy for each o #
def entropyTgOA(f, pYs, pZs, X):
  # posterior distribution for Ys
  ### first calculate pOgY (two-entry matrix)
  pOgY = dict()
  #pOgY = {o:{y:0 for y in iproduct(*pYs)} for o in pO}
  for Y in itertools.product(*pYs):
    for Z in itertools.product(*pZs):
      o = f(*(X+Y+Z))
      p_oGy = prod( (pZ[z] for (z, pZ) in zip(Z, pZs)) )
      if o in pOgY:
        if Y in pOgY[o]:
          pOgY[o][Y] += p_oGy
        else:
          pOgY[o][Y] = p_oGy
      else:
        pOgY[o] = {Y: p_oGy}
      #pOgY[o][Y] += p_oGy#
  
  ### then convert pOgY into pYgO (Bayes)
  # entropiesY stores the entropy of Y given output
  entropiesY = dict()
  pO = pOgA(f, pYs, pZs, X)
  for output in pO: # or in pOgY, equivalently
    for Y in pOgY[output]:
    #for Y in itertools.product(*pYs):
      p_y = prod( (pY[y] for (y, pY) in zip(Y, pYs)) )
      pOgY[output][Y] *= p_y/pO[output]
    entropiesY[output] = entropy(pOgY[output].values())
  return entropiesY

# probability of a vector
# assuming all coordinates are independent (product of probabilities of all lines)
# should be changed to take into account dependency inside A, or inside T
def p_vec(Y, pYs):
  p_y = prod( (pY[y] for (y, pY) in zip(Y, pYs)) )
  return p_y

### jwae(A, T) ###
# joint weighted entropy of T's inputs given A and T
# pYs: "common" prior on T
# pZs: "common" prior on S
def jwae(f, pYs, pZs, A, T):
  pO = pOgAT(f, pZs, A, T)
  entropiesT = entropyTgOA(f, pYs, pZs, A)
  result = sum(pO[output]*entropiesT[output] for output in pO)
  return result

### awae(A) ###
# inefficient way to calculate it, in order to check with old "wae"
def awae(f, pYs, pZs, A):
  print("### please use wae instead of awae ###")
#  pO = pOgA(f, pYs, pZs, A)
#  entropiesT = entropyTgOA(f, pYs, pZs, A)
#  result = sum(pO[output]*entropiesT[output] for output in pO)
  result = sum(p_vec(T, pYs)*jwae(f, pYs, pZs, A, T) for T in iproduct(*pYs))
  return result

### twae(T) ###
# pXs: what T thinks of A
# pYs: what T thinks that A thinks of T
# pZs: what T thinks of S
# T: T's input
def twae(f, pXs, pYs, pZs, T):
  result = sum(p_vec(A, pXs)*jwae(f, pYs, pZs, A, T) for A in iproduct(*pXs))
  return result
